# A solver for thermodynamic binding networks

The thermodynamic binding network (TBN) model
abstracts the thermodynamics of a chemical system
as a tradeoff between maximizing the number of bonds
and maximizing the number of separate complexes.
This package provides a Mathematica interface to a solver
for answering questions about stable configurations of a TBN.

**Take a look at [guide.pdf](guide.pdf) for details.**

## Installing

You will need Linux or MacOS and a C compiler.

1.  Download and build a SAT solver with a standard DIMACS interface like [Glucose Syrup](http://www.labri.fr/perso/lsimon/glucose/). You should now have an executable for it somewhere. Say the path to it is `$SAT`.

2.  Change to the root directory of this package. Build the C code, and create a symbolic link to your SAT solver executable.

        gcc TBN-encode.c -o TBN-encode
        ln -s "$SAT" satsolver

## Running

To use this package, you will also need [Mathematica](https://www.wolfram.com/mathematica). Open SelectedExamplesFromPaper.nb in Mathematica and run it.

## License

This project is licensed under the 3-clause BSD license. See the file LICENSE for details.
