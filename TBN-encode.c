#include <stdio.h>
#include <stdlib.h>

//Uncomment the following line to enable linear at-most-one encoding. Testing showed that this makes performance actually worse for systems we tried, but it might speed up performance for your system. (This is probably causes due to the relative high increase in new variables combined with the relative low reduction in the number of clauses.)
//#define LINEAR_AMO    

struct vertex {
  int index;
  int set;
  int color;
  int size;
  int *edges; };

int nEdge, nVertex, nComp, color, lessStart;
int *setSize;

struct vertex* vertices;

int set (int vertex) {
  int factor = 1000000;
  while ((vertex/factor) == 0)
    factor = factor / 10;
  return vertex / factor; }

int aux (int i) {
  return nEdge + (nComp + 1) * setSize[3] + setSize[3] * (setSize[3] - 1) / 2 + i; }

int rep (int color) {
  return nEdge + color; }

int sum (int i, int j) {
  return nEdge + setSize[3] * i + j; }

int less (int colorA, int colorB) {
  int res = colorB - colorA;
  while (colorA > 1) {
    res += setSize[3] - --colorA; }
  return lessStart + res; }

void addVertex (int vertex) {
  int i;
  for (i = 0; i < nVertex; i++)
    if (vertices[i].index == vertex) return;

  setSize[set (vertex)]++;
  vertices[nVertex].set   = set (vertex);
  vertices[nVertex].color = 0;
  if (vertices[nVertex].set == 3)
    vertices[nVertex].color = color++;

  vertices[nVertex].index = vertex;
  nVertex++; }

int main (int argc, char** argv) {
  int i, j, k, tmp, v, w, maxVertex;
  int *edges;

  maxVertex = 0;
  nVertex   = 0;
  nEdge     = 0;
  color     = 1;

  int edge_alloc   = 8;
  int vertex_alloc = 8;

  edges    = (int*) malloc (sizeof(int) * edge_alloc);
  vertices = (struct vertex*) malloc (sizeof(struct vertex) * vertex_alloc);

  setSize = (int *) malloc (sizeof(int) * 4);
  for (i = 0; i < 4; i++) setSize[i] = 0;

  FILE* graph;

  if (argc < 3) {
    printf ("c wrong number of arguments: ./maxmax GRAPH COMPONENTS\n");
    exit (0); }

  // parse the graph
  graph = fopen (argv[1], "r");
  while (1) {
    tmp = fscanf (graph, " e %i %i ", &v, &w);
    if (tmp != 2) break;
    if (nEdge*2 + 2 >= edge_alloc) {
      edge_alloc *= 2;
      edges = realloc (edges, sizeof(int) * edge_alloc); }
    if (nVertex + 1 >= vertex_alloc) {
      vertex_alloc *= 2;
      vertices = realloc (vertices, sizeof(struct vertex) * vertex_alloc); }
    edges[nEdge*2  ] = v;
    edges[nEdge*2+1] = w;
    addVertex (v);
    addVertex (w);
    if (v > maxVertex) maxVertex = v;
    if (w > maxVertex) maxVertex = w;
    nEdge++; } ;
  fclose (graph);

  nComp = atoi(argv[2]);

  // create a mapping from vertices to the position in the array
  int *map;
  map = (int *) malloc (sizeof(int) * (maxVertex + 1));
  for (i = 0; i <= maxVertex; i++) map[i] = -1;
  for (i = 0; i < nVertex; i++)
    map [vertices[i].index] = i;

  // filter out the edges that are connect to V3
  tmp = 0;
  for (j = 0; j < nEdge; j++) {
    int v = edges[2*j  ];
    int w = edges[2*j+1];
    if (set(v) == 3) {
      vertices[map[w]].color = vertices[map[v]].color;
      continue; }
    if (set(w) == 3) {
      vertices[map[v]].color = vertices[map[w]].color;
      continue; }
    edges[2*tmp  ] = v;
    edges[2*tmp+1] = w;
    tmp++;
    printf("c %i e %i %i\n", tmp, v, w); }
  nEdge = tmp;

  for (i = 0; i < nVertex; i++) {
    int size = 0;
    for (j = 0; j < nEdge; j++) {
      if (edges[2*j  ] == vertices[i].index) size++;
      if (edges[2*j+1] == vertices[i].index) size++; }
    vertices[i].size = size;
    vertices[i].edges = (int *) malloc (sizeof(int) * size);

    size = 0;
    for (j = 0; j < nEdge; j++) {
      if (edges[2*j  ] == vertices[i].index) vertices[i].edges[size++] = j;
      if (edges[2*j+1] == vertices[i].index) vertices[i].edges[size++] = j; } }

  //  printf("c set sizes: %i %i %i\n", setSize[1], setSize[2], setSize[3]);

  int minset = 1;
  if (setSize[1] > setSize[2]) minset = 2;

  int nCls = 4 + 2 * nComp * (setSize[3] - 1) + (nComp - 1) * (setSize[3] - 1);
  nCls += setSize[3] * (setSize[3] - 1) / 2 + setSize[3] * (setSize[3] - 1) * (setSize[3] - 2) / 3;
  if (nComp == 1) nCls--;

  for (i = 0; i < nVertex; i++) {
    if (vertices[i].set != minset) continue;
    int flag = 0;
    for (j = 0; j < vertices[i].size; j++) flag = 1;
    if (flag) nCls++; }

  for (i = 0; i < nVertex; i++)
    if (vertices[i].set != 3)
      for (j = 0; j < vertices[i].size; j++)
        for (k = j + 1; k < vertices[i].size; k++) nCls++;

  for (j = 0; j < nEdge; j++) {
    int vi = map[edges[2*j  ]];
    int wi = map[edges[2*j+1]];
    if (vertices[vi].color == 0 || vertices[wi].color == 0) continue;
    if (vertices[vi].color > vertices[wi].color) nCls++;
    if (vertices[wi].color > vertices[vi].color) nCls++; }

  lessStart = nEdge + (nComp + 1) * setSize[3];

  printf("p cnf %i %i\n", lessStart + setSize[3] * (setSize[3] - 1) / 2, nCls);

  for (i = 0; i < nVertex; i++) {
    if (vertices[i].set != minset) continue;
    int flag = 0;
    for (j = 0; j < vertices[i].size; j++) {
      printf ("%i ", vertices[i].edges[j] + 1);
      flag = 1; }
    if (flag) printf("0\n"); }

  int extra = 1;
  // At most one edge of each vertex in V1 and V2 can be alive
  for (i = 0; i < nVertex; i++)
    if (vertices[i].set != 3) { // maybe only store the vertices in V1 and V2
#ifndef LINEAR_AMO
      for (j = 0; j < vertices[i].size; j++)
        for (k = j + 1; k < vertices[i].size; k++)
          printf("-%i -%i 0\n", vertices[i].edges[j] + 1, vertices[i].edges[k] + 1);
#else
      if (vertices[i].size <= 4) {
        for (j = 0; j < vertices[i].size; j++)
          for (k = j + 1; k < vertices[i].size; k++)
            printf("-%i -%i 0\n", vertices[i].edges[j] + 1, vertices[i].edges[k] + 1); }
      else {
        printf("%i %i 0\n", -vertices[i].edges[0] - 1, -vertices[i].edges[1] - 1);
        printf("%i %i 0\n", -vertices[i].edges[0] - 1, -vertices[i].edges[2] - 1);
        printf("%i %i 0\n", -vertices[i].edges[1] - 1, -vertices[i].edges[2] - 1);
        printf("%i %i 0\n", -vertices[i].edges[0] - 1, -aux(extra));
        printf("%i %i 0\n", -vertices[i].edges[1] - 1, -aux(extra));
        printf("%i %i 0\n", -vertices[i].edges[2] - 1, -aux(extra));
        for (j = 3; j < vertices[i].size - 3; j += 2) {
          printf("%i %i 0\n",  aux(extra),                 -vertices[i].edges[j  ] - 1);
          printf("%i %i 0\n",  aux(extra),                 -vertices[i].edges[j+1] - 1);
          printf("%i %i 0\n", -vertices[i].edges[j  ] - 1, -vertices[i].edges[j+1] - 1);
          printf("%i %i 0\n",  aux(extra),                 -aux(extra+1));
          printf("%i %i 0\n", -vertices[i].edges[j  ] - 1, -aux(extra+1));
          printf("%i %i 0\n", -vertices[i].edges[j+1] - 1, -aux(extra+1));
          extra++; }
        printf("%i %i 0\n", aux(extra),                  -vertices[i].edges[j  ] - 1);
        printf("%i %i 0\n", aux(extra),                  -vertices[i].edges[j+1] - 1);
        printf("%i %i 0\n", -vertices[i].edges[j  ] - 1, -vertices[i].edges[j+1] - 1);
        if (j + 1 < vertices[i].size - 1) {
          printf("%i %i 0\n", aux(extra),                  -vertices[i].edges[j+2] - 1);
          printf("%i %i 0\n", -vertices[i].edges[j  ] - 1, -vertices[i].edges[j+2] - 1);
          printf("%i %i 0\n", -vertices[i].edges[j+1] - 1, -vertices[i].edges[j+2] - 1); }
      extra++; }
#endif
  }

  // pair implies bind
  for (j = 0; j < nEdge; j++) {
    int vi = map[edges[2*j  ]];
    int wi = map[edges[2*j+1]];
    if (vertices[vi].color == 0 || vertices[wi].color == 0) continue;
    if (vertices[vi].color > vertices[wi].color)
      printf("-%i %i 0\n", j+1, less(vertices[wi].color,vertices[vi].color));
    if (vertices[wi].color > vertices[vi].color)
      printf("-%i %i 0\n", j+1, less(vertices[vi].color,vertices[wi].color)); }

  // make the lower left corner equal to the first representative
  printf("%i -%i 0\n", rep (1), sum (1, 1));
  printf("-%i %i 0\n", rep (1), sum (1, 1));
  if (nComp > 1) printf("-%i 0\n", sum (2, 1));
  printf("%i 0\n", sum (nComp, setSize[3]));

  // horizontal implications and increment implications
  for (i = 1; i <= nComp; i++)
    for (j = 1; j < setSize[3]; j++) {
      printf("-%i %i 0\n", sum (i, j), sum (i, j+1));
      printf("%i %i -%i 0\n", rep (j+1), sum (i, j), sum (i, j+1)); }

  // diagonal implications
  for (i = 1; i < nComp; i++)
    for (j = 1; j < setSize[3]; j++)
      printf("%i -%i 0\n", sum (i, j), sum (i+1, j+1));

  // i < j implies j not a representative
  for (i = 1; i <= setSize[3]; i++)
    for (j = i + 1; j <= setSize[3]; j++)
      printf("-%i -%i 0\n", less (i, j), rep (j));

  // addd transitive relationship between < variables
  for (i = 1; i <= setSize[3]; i++)
    for (j = i + 1; j <= setSize[3]; j++)
      for (k = j + 1; k <= setSize[3]; k++) {
        printf("%i -%i -%i 0\n", less (i, j), less (i, k), less (j, k));
        printf("-%i %i -%i 0\n", less (i, j), less (i, k), less (j, k)); }
}
