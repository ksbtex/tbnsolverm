(* ::Package:: *)

(* ::Text:: *)
(*Note: We assume that executables are in a specific place relative to the location of the package.  You may have to change "Define locations of files" below to set up on your machine.*)


BeginPackage["TBNSolver`"];


(* ::Subsection:: *)
(*Definitions to export*)


StableConfiguration::usage = 
" StableConfiguration[\!\(\*
StyleBox[\"tbn\",\nFontSlant->\"Italic\"]\), <\!\(\*
StyleBox[\"lower\",\nFontSlant->\"Italic\"]\)>] returns the polymers (as a set of monomers) of a stable configuration of thermodynamic binding network \!\(\*
StyleBox[\"tbn\",\nFontSlant->\"Italic\"]\). \
Note that only one of potentially many stable configurations is returned. \
The optional parameter \!\(\*
StyleBox[\"lower\",\nFontSlant->\"Italic\"]\) (default value 1) provides a lower bound for the number of polymers in the stable configuration. \
The output is {} if there is no stable configuration with at least \!\(\*
StyleBox[\"lower\",\nFontSlant->\"Italic\"]\) polymers.
";

StableConfigurationWithFree::usage = 
" StableConfigurationWithFree[\!\(\*
StyleBox[\"tbn\",\nFontSlant->\"Italic\"]\), \!\(\*
StyleBox[\"free\",\nFontSlant->\"Italic\"]\), <\!\(\*
StyleBox[\"lower\",\nFontSlant->\"Italic\"]\)>] returns the polymers (as a set of monomers) of a stable configuration of thermodynamic binding network \!\(\*
StyleBox[\"tbn\",\nFontSlant->\"Italic\"]\) \
where monomer \!\(\*
StyleBox[\"free\",\nFontSlant->\"Italic\"]\) is free (unbound to any other monomer). \
If such a stable configuration does not exist, the function returns {}. \
The optional parameter \!\(\*
StyleBox[\"lower\",\nFontSlant->\"Italic\"]\) (default value 1) provides a lower bound for the number of polymers in the stable configuration.";
StableConfigurationWithFree::tbnWithoutFree = 
"  StableConfigurationWithFree[\!\(\*
StyleBox[\"tbn\",\nFontSlant->\"Italic\"]\), \!\(\*
StyleBox[\"free\",\nFontSlant->\"Italic\"]\)]: Monomer \!\(\*
StyleBox[\"free\",\nFontSlant->\"Italic\"]\) must be contained in \!\(\*
StyleBox[\"tbn\",\nFontSlant->\"Italic\"]\)";

StableConfigurationWithTogether::usage = 
" StableConfigurationWithTogether[\!\(\*
StyleBox[\"tbn\",\nFontSlant->\"Italic\"]\), \!\(\*
StyleBox[\"monomer1\",\nFontSlant->\"Italic\"]\), \!\(\*
StyleBox[\"monomer2\",\nFontSlant->\"Italic\"]\), <\!\(\*
StyleBox[\"lower\",\nFontSlant->\"Italic\"]\)>] returns the polymers (as a set of monomers) of a stable configuration of thermodynamic binding network \!\(\*
StyleBox[\"tbn\",\nFontSlant->\"Italic\"]\) \
where \!\(\*
StyleBox[\"monomer1\",\nFontSlant->\"Italic\"]\) and \!\(\*
StyleBox[\"monomer2\",\nFontSlant->\"Italic\"]\) are in the same polymer. \
If such a stable configuration does not exist, the function returns {}. \
The optional parameter \!\(\*
StyleBox[\"lower\",\nFontSlant->\"Italic\"]\) (default value 1) provides a lower bound for the number of polymers in the stable configuration.";
StableConfigurationWithTogether::tbnWithoutMonomers = 
" StableConfigurationWithTogether[\!\(\*
StyleBox[\"tbn\",\nFontSlant->\"Italic\"]\), \!\(\*
StyleBox[\"monomer1\",\nFontSlant->\"Italic\"]\), \!\(\*
StyleBox[\"monomer2\",\nFontSlant->\"Italic\"]\), <\!\(\*
StyleBox[\"lower\",\nFontSlant->\"Italic\"]\)>]: The monomers \!\(\*
StyleBox[\"monomer1\",\nFontSlant->\"Italic\"]\) and \!\(\*
StyleBox[\"monomer2\",\nFontSlant->\"Italic\"]\) must be contained in \!\(\*
StyleBox[\"tbn\",\nFontSlant->\"Italic\"]\).";

NumberOfSaturatedConfigurations::usage = "Computes the number of saturated configurations of the given tbn";


(* ::Subsection:: *)
(*Private*)


Begin["`Private`"];


(* Define locations of files *)
(* FIXME: NotebookDirectory[] gets the directory of the notebook
	that loads this package, which would not always be the same
	as the directory containing this package. *)

SetDirectory[NotebookDirectory[]];
SATGeneratorExecutable = "TBN-encode";
SATSolverExecutable = "satsolver";

triEncFile = "te.txt"; (* tripartite encoding *)

star=Global`star;   (* we want star[] to refer to star[] in the Global context *)


(* Check if all executable files exist at the specified locations. If not, abort. *)
CheckExecutablesExist[]:=(
	If[Not[FileExistsQ[SATGeneratorExecutable]], Print["Missing SATGeneratorExecutable; expected at: "<>SATGeneratorExecutable]; Abort[]];
	If[Not[FileExistsQ[SATSolverExecutable]], Print["Missing SATSolverExecutable; expected at: "<>SATSolverExecutable]; Abort[]];
)


(* Compute the number of saturated configurations *)
domTerm[domCount_,starDomCount_]:=
  Block[
   {min=Min[domCount,starDomCount],
    max=Max[domCount,starDomCount]},
   max!/(max-min)!]

NumberOfSaturatedConfigurations[tbn_]:=
 Module[{rawdomains,ftbn=Flatten[tbn]},
   (* get all domain types, removing star wrappers *)
   rawdomains=DeleteDuplicates[Cases[ftbn,star[x_]|x_:>x]]; 
   Times@@Map[domTerm[Count[ftbn,#],Count[ftbn,star[#]]]&,rawdomains]]


TripartiteEncoding[tbn_,outputFile_] := 
  Module[{numMonomers,configNumbers,verts,vertices,newVertices,uniqueSymbols,getSymbol,getExpressions,makeEdge12,isStarred,
  makeEdge3,getAll12Edges,getAll3Edges,all12Edges,allEdges},
	numMonomers = Length@tbn;
	(*Replace each occurrence of every symbol in the tbn with a unique number while maintaining the structure. This will be useful in finding the internal edges*)
	i = 0;
	configNumbers = Replace[tbn, _ :> ++i, {2}];
	verts = Flatten[tbn];
	vertices = Range[Length[verts]];
	newVertices = Range[Length[verts] + 1, Length[verts] + numMonomers];
	(*Get all the unique domains*) 
	uniqueSymbols = DeleteDuplicates[Cases[verts, star[_]] /. star -> Identity];
	getSymbol[z_] := Function[{i}, Not[FreeQ[verts[[i]], z]]];
	(*get the domains that contain a particular symbol*) 
	getExpressions[h_] := Select[vertices, getSymbol[h]];
	makeEdge12[{x_, y_}] := "e " <> "1" <> IntegerString[x] <> " 2" <> IntegerString[y];
	isStarred[x_] := Head[verts[[x]]] === star;
	makeEdge3[{x_, y_}] := If[isStarred[x], "e " <> "2" <> IntegerString[x] <> " 3" <> IntegerString[y], "e " <> "1" <> IntegerString[x] <> " 3" <> IntegerString[y]];
	getAll12Edges[x_] := Module[{strands, separatedStrands, edgePairs}, strands = getExpressions[x];
	(*Get all strands containing the given domain*) 
	separatedStrands = {Select[strands, Not@*isStarred], Select[strands, isStarred]};
	(*Separate the starred and unstarred ones*) 
	edgePairs = Tuples[separatedStrands];
	Map[makeEdge12, edgePairs]];
	getAll3Edges[x_] := Module[{domains, edgePairs}, domains = configNumbers[[x]];
	(*Get all strands containing the given domain*) 
	edgePairs = Tuples[{domains, {newVertices[[x]]}}];
	Map[makeEdge3, edgePairs]];
	all3Edges = Flatten[getAll3Edges /@ Range[Length@newVertices], 1];
	all12Edges = Flatten[getAll12Edges /@ uniqueSymbols, 1];
	allEdges = Join[all12Edges, all3Edges];
	Export[outputFile, allEdges];
  ]


(* Note: uses all3Edges global variable generated in TripartiteEncoding *)
GetComponents[tbn_,filecnf_,fileout_] := 
  Module[{verts,r,numAllEdges,selectedEdges,matchingEdges,all3Edgesg,allStableEdges,connectedComponents},
    verts = Flatten[tbn];
	r = ReadList[filecnf, String];
	numAllEdges = Length@Select[r, StringStartsQ[#, RegularExpression["c"]] &];
	selectedEdges = Select[ReadList[fileout, Number], (# <= numAllEdges && # > 0) &];
	matchingEdges = ToExpression[Map[StringReplace[#, RegularExpression["c [\\d]+ e [\\d]([\\d]+) [\\d]([\\d]+)"] -> "$1<->$2"] &, r[[selectedEdges]]]];
	all3Edgesg = ToExpression[Map[StringReplace[#, RegularExpression["e [\\d]([\\d]+) [\\d]([\\d]+)"] -> "$1<->$2"] &, all3Edges]];
	allStableEdges = Join[matchingEdges, all3Edgesg];
	connectedComponents = ConnectedComponents[Graph[allStableEdges]];
	tbn[[# - Length[verts]]] & /@ Select[#, # > Length[verts] &] & /@ connectedComponents
  ]


(* FIXME: SAT encoder sometimes returns an unsatisfiable CNF if star domains are not limiting.
This function generates replace rules to make starred domains limiting.
Note: apply the same rules to restore original system.
 *)
StandardizeTBNRules[tbn_]:=
  Module[{rawdomains,ftbn=Flatten[tbn]},
	(* get all domain types, removing star wrappers *)
	rawdomains=DeleteDuplicates[Cases[ftbn,star[x_]|x_:>x]]; 
	Flatten[Map[
		If[Count[ftbn,#]<Count[ftbn,star[#]],
			{star[#]->#,#->star[#]},
			{}]&,
	rawdomains]]]


(* get complement of domain dom *)
complement[star[dom_]]:=dom
complement[dom_]:=star[dom]

(* give a list of the domains in tbn that are in excess *)
getExcessDomains[tbn_]:=
  Module[{rawdomains,ftbn=Flatten[tbn]},
	(* get all domain types, removing star wrappers *)
	rawdomains=DeleteDuplicates[Cases[ftbn,star[x_]|x_:>x]]; 
	Reap[Map[
			(
				If[Count[ftbn,#]<Count[ftbn,star[#]],Sow[star[#]]];
				If[Count[ftbn,#]>Count[ftbn,star[#]],Sow[#]]
			)&,
			rawdomains]
	]//Last//Flatten
  ]

(* In Join[tbn,{freeMonomer}] is there a saturated configuration with freeMonomer free? *) 
CanBeFreeQ[tbn_,freeMonomer_]:=
  Module[{excessDomsOfTbn=getExcessDomains[tbn]},
	Not[Or@@Map[MemberQ[excessDomsOfTbn,complement[#]]&,freeMonomer]]]


StableConfiguration[tbnArg_, startNum_:1] := 
  Module[{tbn,standardizeRules,numpoly,filecnf,fileout,satsolveroutput,lastSATfilecnf, lastSATfileout},
  
	CheckExecutablesExist[];  (* terminate if executables don't exist *)
	
	standardizeRules=StandardizeTBNRules[tbnArg];
	tbn=tbnArg/.standardizeRules;
	
	TripartiteEncoding[tbn, triEncFile];    (* create tripartite graph encoding file at triEncFile *)
	numpoly = startNum;
	
	While[True,
		PrintTemporary["Trying number of polymers: " <> ToString[numpoly]];
		filecnf = CreateFile[];   (* the CNF which is input to SAT solver*)
		fileout = CreateFile[];   (* output of SAT solver *)
		PrintTemporary["Generating cnf..."];
		(* Generate CNF from tripartite graph encoding *)
		Export[filecnf,RunProcess[{SATGeneratorExecutable, triEncFile, ToString[numpoly]},"StandardOutput"],"String"];
		PrintTemporary["Solving..."];
		(* Run SAT solver on CNF *)
		RunProcess[{SATSolverExecutable, filecnf, fileout}];
		satsolveroutput=ToString[ReadString[fileout]];
		If[satsolveroutput=="EndOfFile",Print["Error: Solver returned empty file"];Abort[]];
		If[satsolveroutput=="UNSAT\n",Break[]];
		lastSATfilecnf = filecnf;
		lastSATfileout = fileout;
		numpoly += 1;
	];
	
	If[numpoly==startNum,Return[{}]];  (* No  satisfying assignment found even with startNum polymers. *)
	
	(* Extract the stable configuration from filecnf and fileout for the last satisfiable CNF*)
	GetComponents[tbn,lastSATfilecnf,lastSATfileout]/.standardizeRules
  ]


StableConfigurationWithFree[tbn_, freeMonomer_, startNum_:1] := 
  Module[{tbnWithoutOutputMonomer,stableConfigWithOutputMonomer,stableConfigWithoutOutputMonomer},
    If[!MemberQ[tbn,freeMonomer,{1}],Message[StableConfigurationWithFree::tbnWithoutFree];Abort[]]; (* tbn is supposed to contain freeMonomer *) 
    tbnWithoutOutputMonomer = DeleteCases[tbn,freeMonomer,{1},1];  (* remove freeMonomer from tbn (remove only 1 copy if multiple copies) *)
    If[!CanBeFreeQ[tbnWithoutOutputMonomer,freeMonomer],Return[{}]];  (* if freeMonomer cannot be free in any saturated config of tbn, return false *)
    stableConfigWithOutputMonomer = StableConfiguration[tbn,startNum];
    If[stableConfigWithOutputMonomer=={},Print["No stable configuration of tbn found with \[GreaterEqual] ", ToString[startNum], " polymers. Try decreasing startNum."];Abort[]];
    stableConfigWithoutOutputMonomer = 
      StableConfiguration[
        tbnWithoutOutputMonomer,
        Length[stableConfigWithOutputMonomer]-1]; (* check S(tbn-freeMonomer) \[GreaterEqual] S(tbn) - 1 *)
    (* Is there a stable configuration of tbn-freeMonomer with at least S(tbn) - 1 polymers? *)
    If[stableConfigWithoutOutputMonomer=={},{},Append[stableConfigWithoutOutputMonomer,{freeMonomer}]]
  ]


(* FIXME: This will probably not work if monomer1==monomer2 *)
StableConfigurationWithTogether[tbn_, monomer1_, monomer2_, startNum_:1] := 
  Module[{modifiedTbn=tbn, domx=Unique[], domy=Unique[], outputStableConfig},
	If[!ContainsAll[tbn,{monomer1,monomer2}],Message[StableConfigurationWithTogether::tbnWithoutMonomers];Abort[]]; (* tbn is supposed to contain monomer1 and monomer2 *)
	(* remove monomer1 and monomer2 from tbn *)
	modifiedTbn=DeleteCases[modifiedTbn,monomer1,{1},1];
	modifiedTbn=DeleteCases[modifiedTbn,monomer2,{1},1];
	(* modify tbn and all StableConfigurationWithFree *)
	outputStableConfig=StableConfigurationWithFree[
		Join[modifiedTbn,{Append[monomer1,domx],Append[monomer2,domy], {domx,domy},{star[domx],star[domy]}}],
		{domx,domy},
		startNum];
	(* If {domx,domy} can't be stably free, then monomer1 and monomer2 cannot be stably together *)
	If[outputStableConfig=={}, Return[{}]];
	(* Otherwise, remove domx, domy, {domx,domy},{star[domx],star[domy]} to get the stable configuration of the original tbn*)
	outputStableConfig = DeleteCases[outputStableConfig, {{domx,domy}}, {1}, 1];
	outputStableConfig = DeleteCases[outputStableConfig, {star[domx], star[domy]}, {2}, 1];
	outputStableConfig = DeleteCases[outputStableConfig, domx|domy, {3}];
	outputStableConfig
  ]


End[];
EndPackage[];
